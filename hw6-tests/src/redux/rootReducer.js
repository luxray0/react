// Core
import { combineReducers } from "redux";

// Reducers

import { dataReducer as data } from "./reducers/products";
import { cartReducer as cart } from "./reducers/cart";
import { favoriteReducer as favorite } from "./reducers/favorites";
import { modalReducer as modal } from "./reducers/modal";

export const rootReducer = combineReducers({ data, cart, favorite, modal });
