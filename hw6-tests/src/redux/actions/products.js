import { dataTypes } from "../types";
import axios from 'axios';

export const setData = (data) => {
    return {
        type: dataTypes.SET_PRODUCTS,
        payload: data,
    };
}
export const getDataAsync = () => {
    return function (dispatch) {
        dispatch({ type: dataTypes.SET_PRODUCTS });

        axios
            .get('./data/products.json')
            .then((response) => {
                const results = response.data.products;
                dispatch(setData(results));
            })
            .catch((err) => {
                console.error('Error fetching data:', err);
            });
    };
};