import { isModalOpen } from "../types";

export const setModalsOpen = (isOpen) => {
    return (dispatch) => {
        dispatch({
            type: isModalOpen.SET_MODAL_OPEN,
            payload: isOpen
        });
    };
};