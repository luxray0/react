import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import Button from "../Components/ConfirmModal/Button";

describe("Button", () => {
    test("should render the button with the correct text", async () => {
        const buttonText = "Add to cart";
        render(<Button text={buttonText} />);

        const button = screen.getByText(buttonText);
        expect(button).toBeInTheDocument();
    });

    test("should call the onClick function when the button is clicked", async () => {
        const onClickMock = jest.fn();
        render(<Button onClick={onClickMock} />);

        const button = screen.getByTestId("button");
        fireEvent.click(button);

        expect(onClickMock).toHaveBeenCalled();
    });

    test("should disable the button when the text is 'Added!'", async () => {
        render(<Button text="Added!" />);
        const button = screen.getByText("Added!");

        expect(button).toBeDisabled();
    });

    test("should display the checkmark SVG when the text is 'Added!'", async () => {
        render(<Button text="Added!" />);
        const checkmarkSvg = screen.getByTestId("checkmark-svg");

        expect(checkmarkSvg).toBeInTheDocument();
        expect(checkmarkSvg).toHaveAttribute("viewBox", "0 0 24 24");
    });

});