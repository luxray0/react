import { render } from '@testing-library/react';
import Modal from '../../Components/ConfirmModal/Modal';

test('renders Modal component correctly', () => {
    const header = 'Modal Header';
    const closeButton = true;
    const text = 'Modal content text';
    const actions = <button>Button</button>;
    const className = 'custom-modal';
    const onClose = jest.fn();

    const { container } = render(
        <Modal
            header={header}
            closeButton={closeButton}
            text={text}
            actions={actions}
            className={className}
            onClose={onClose}
        />
    );

    expect(container).toMatchSnapshot();
});
