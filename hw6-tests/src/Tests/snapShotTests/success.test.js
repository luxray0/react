import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import Success from '../../Pages/Success';

test('renders Success component correctly', () => {
    const { container } = render(
        <Router>
            <Success />
        </Router>
    );

    expect(container).toMatchSnapshot();
});
