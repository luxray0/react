import { render } from '@testing-library/react';
import Button from '../../Components/ConfirmModal/Button';

test('renders Button component correctly', () => {
    const backgroundColor = '#ff0000';
    const onClick = jest.fn();
    const className = 'custom-button';
    const text = 'Click me';
    const onMouseEnter = jest.fn();
    const onMouseLeave = jest.fn();

    const { container } = render(
        <Button
            backgroundColor={backgroundColor}
            onClick={onClick}
            className={className}
            text={text}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
        />
    );

    expect(container).toMatchSnapshot();
});
