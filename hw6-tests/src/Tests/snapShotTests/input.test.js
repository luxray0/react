import { render } from '@testing-library/react';
import Input from '../../Components/ShoppingForm/Input';

test('renders Input component correctly', () => {
    const formErrors = {
        input1: 'Error 1',
        input2: 'Error 2',
    };
    const inputName = 'input1';
    const inputPlaceholder = 'Enter value';
    const inputValue = 'Example value';
    const onChange = jest.fn();

    const { container } = render(
        <Input
            formErrors={formErrors}
            inputName={inputName}
            inputPlaceholder={inputPlaceholder}
            inputValue={inputValue}
            onChange={onChange}
        />
    );

    expect(container).toMatchSnapshot();
});