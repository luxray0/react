import { render } from '@testing-library/react';
import Loader from '../../Components/Loaders';

test('renders Loader component correctly', () => {
    const { container } = render(<Loader />);

    expect(container).toMatchSnapshot();
});