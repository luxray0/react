import React from 'react';
import { render, fireEvent, screen } from "@testing-library/react";
import '@testing-library/jest-dom'
import Modal from '../Components/ConfirmModal/Modal';

describe('Modal', () => {
    const headerTest = 'header';
    const closeButtonTest = 'closeButton';
    const contentTest = 'content';
    const actionsTest = 'actions';

    test('should render the modal with header, content, and actions', async () => {
        render(
            <Modal
                header={headerTest}
                closeButton
                text={contentTest}
                actions={<div>{actionsTest}</div>}
            />
        );

        const header = screen.getByTestId(headerTest);
        const closeButton = screen.getByTestId(closeButtonTest);
        const content = screen.getByTestId(contentTest);
        const actions = screen.getByTestId(actionsTest);

        expect(header).toBeInTheDocument();
        expect(closeButton).toBeInTheDocument();
        expect(content).toBeInTheDocument();
        expect(actions).toBeInTheDocument();
    });

    test('should call onClose function when close button is clicked', async () => {
        const onCloseMock = jest.fn();
        render(
            <Modal header={headerTest} closeButton text={contentTest} onClose={onCloseMock} />
        );

        const closeButton = screen.getByTestId(closeButtonTest);
        fireEvent.click(closeButton);

        expect(onCloseMock).toHaveBeenCalled();
    });
});