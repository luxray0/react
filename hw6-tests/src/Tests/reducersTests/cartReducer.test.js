import { cartReducer } from '../../redux/reducers/cart';
import { addCartItem, removeCartItem, clearCart } from '../../redux/types';

describe('cartReducer', () => {
    test('handles ADD_TO_CART action correctly', async () => {
        const initialState = {
            cartItems: {
                counterItems: [],
                buttonText: {}
            }
        };

        const action = {
            type: addCartItem.ADD_TO_CART,
            payload: {
                counterItems: { id: 1, name: 'LEGO Marvel Nanogauntlet' }
            }
        };

        const newState = cartReducer(initialState, action);

        expect(newState.cartItems.counterItems).toEqual([{ id: 1, name: 'LEGO Marvel Nanogauntlet' }]);
        expect(newState.cartItems.buttonText).toEqual({ 1: 'Added!' });
    });

    test('handles REMOVE_FROM_CART action correctly', async () => {
        const initialState = {
            cartItems: {
                counterItems: [{ id: 1, name: 'LEGO Marvel Nanogauntlet' }],
                buttonText: { 1: 'Added!' }
            }
        };

        const action = {
            type: removeCartItem.REMOVE_FROM_CART,
            payload: {
                counterItems: { id: 1, name: 'LEGO Marvel Nanogauntlet' }
            }
        };

        const newState = cartReducer(initialState, action);

        expect(newState.cartItems.counterItems).toEqual([]);
        expect(newState.cartItems.buttonText).toEqual({ 1: 'Add to cart' });
    });

    test('handles CLEAR_CART action correctly', async () => {
        const initialState = {
            cartItems: {
                counterItems: [{ id: 1, name: 'LEGO Marvel Nanogauntlet' }],
                buttonText: { 1: 'Added!' }
            }
        };

        const action = {
            type: clearCart.CLEAR_CART
        };

        const newState = cartReducer(initialState, action);

        expect(newState.cartItems.counterItems).toEqual([]);
        expect(newState.cartItems.buttonText).toEqual({});
    });

    test('returns initial state for unknown action type', async () => {
        const initialState = {
            cartItems: {
                counterItems: [],
                buttonText: {}
            }
        };

        const action = {
            type: 'UNKNOWN_ACTION',
            payload: {}
        };

        const newState = cartReducer(initialState, action);

        expect(newState).toEqual(initialState);
    });
});
