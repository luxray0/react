import { favoriteReducer } from '../../redux/reducers/favorites';
import { addFavoriteItem, removeFavoriteItem } from '../../redux/types';

describe('favoriteReducer', () => {
    test('handles ADD_TO_FAVORITE action correctly', async () => {
        const initialState = {
            favoriteItems: {
                counterFav: [],
                favoriteButtonColor: {}
            }
        };

        const action = {
            type: addFavoriteItem.ADD_TO_FAVORITE,
            payload: {
                counterFav: { id: 1, name: 'LEGO Marvel Nanogauntlet' }
            }
        };

        const newState = favoriteReducer(initialState, action);

        expect(newState.favoriteItems.counterFav).toEqual([{ id: 1, name: 'LEGO Marvel Nanogauntlet' }]);
        expect(newState.favoriteItems.favoriteButtonColor).toEqual({ 1: '#6ba6ff' });
    });

    test('handles REMOVE_FROM_FAVORITE action correctly', async () => {
        const initialState = {
            favoriteItems: {
                counterFav: [{ id: 1, name: 'LEGO Marvel Nanogauntlet' }],
                favoriteButtonColor: { 1: '#6ba6ff' }
            }
        };

        const action = {
            type: removeFavoriteItem.REMOVE_FROM_FAVORITE,
            payload: {
                counterFav: { id: 1, name: 'LEGO Marvel Nanogauntlet' }
            }
        };

        const newState = favoriteReducer(initialState, action);

        expect(newState.favoriteItems.counterFav).toEqual([]);
        expect(newState.favoriteItems.favoriteButtonColor).toEqual({ 1: '#fff' });
    });

    test('returns initial state for unknown action type', () => {
        const initialState = {
            favoriteItems: {
                counterFav: [],
                favoriteButtonColor: {}
            }
        };

        const action = {
            type: 'UNKNOWN_ACTION',
            payload: {}
        };

        const newState = favoriteReducer(initialState, action);

        expect(newState).toEqual(initialState);
    });
});