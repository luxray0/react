import { modalReducer } from '../../redux/reducers/modal';
import { isModalOpen } from '../../redux/types';

describe('modalReducer', () => {
    test('handles SET_MODAL_OPEN action correctly', async () => {
        const initialState = {
            isModalOpen: false
        };

        const action = {
            type: isModalOpen.SET_MODAL_OPEN,
            payload: true
        };

        const newState = modalReducer(initialState, action);

        expect(newState.isModalOpen).toEqual(true);
    });

    test('returns initial state for unknown action type', async () => {
        const initialState = {
            isModalOpen: false
        };

        const action = {
            type: 'UNKNOWN_ACTION',
            payload: {}
        };

        const newState = modalReducer(initialState, action);

        expect(newState).toEqual(initialState);
    });
});