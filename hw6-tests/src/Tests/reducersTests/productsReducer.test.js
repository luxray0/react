import { dataReducer } from '../../redux/reducers/products';
import { dataTypes } from '../../redux/types';

describe('dataReducer', () => {
    test('handles SET_PRODUCTS action correctly', () => {
        const initialState = {
            products: []
        };

        const products = [
            { id: 1, name: 'LEGO Marvel Nanogauntlet' },
            { id: 2, name: 'LEGO Harry Potter Hogwarts' }
        ];

        const action = {
            type: dataTypes.SET_PRODUCTS,
            payload: products
        };

        const newState = dataReducer(initialState, action);

        expect(newState.products).toEqual(products);
    });

    test('returns initial state for unknown action type', () => {
        const initialState = {
            products: []
        };

        const action = {
            type: 'UNKNOWN_ACTION',
            payload: {}
        };

        const newState = dataReducer(initialState, action);

        expect(newState).toEqual(initialState);
    });
});