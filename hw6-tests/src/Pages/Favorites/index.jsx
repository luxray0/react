import PropTypes from "prop-types";
import ProductCard from "../../Components/ProductCard";
import Loader from "../../Components/Loaders";

import { useEffect } from "react";
import { Link } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { removeFromFavorite } from "../../redux/actions/favorites";
function Favorites(props) {
  const { favoriteState } = props;

  const dispatch = useDispatch();

  useEffect(() => {
    localStorage.setItem("favoriteState", JSON.stringify(favoriteState));
  }, [favoriteState]);

  return (
    <>
      {favoriteState.counterFav?.length ? (
        <div className="favs">
          <h2 className="favs__title">Favorites</h2>
          <div className="favs__wrapper">
            {favoriteState.counterFav?.map((product) => (
              <div key={product.id} className="favs__card">
                <ProductCard
                  name={product.name}
                  article={product.article}
                  url={product.url}
                  color={product.color}
                  price={product.price}
                  favoriteStatus="#6ba6ff"
                  handleAddFavorite={() =>
                    dispatch(removeFromFavorite(product))
                  }
                  buttonClass={"none"}
                />
              </div>
            ))}
          </div>
          <Link to={"/"}>
            <button className="back__btn"> Continue Shopping</button>
          </Link>
        </div>
      ) : (
        <>
          <h2>No products have been added yet</h2>
          <Loader />
        </>
      )}
    </>
  );
}

const mapStateToProps = (state) => ({
  favoriteState: state.favorite.favoriteItems,
});

export default connect(mapStateToProps)(Favorites);

Favorites.propTypes = {
  favoriteState: PropTypes.shape({
    counterFav: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        article: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
      })
    ),
  }).isRequired,
  removeFromFavorite: PropTypes.func.isRequired,
};

Favorites.defaultProps = {
  favoriteState: {
    counterFav: [],
  },
  removeFromFavorite: () => {},
};
