import PropTypes from "prop-types";
import ProductCard from "../../Components/ProductCard";
import Loader from "../../Components/Loaders";

import { useEffect, useContext } from "react";
import { connect, useDispatch } from "react-redux";
import { getDataAsync } from "../../redux/actions/products";
import { addToCart } from "../../redux/actions/cart";
import {
  addToFavorite,
  removeFromFavorite,
} from "../../redux/actions/favorites";
import { ToggleViewContext } from "../../Context/contexts";

function Home(props) {
  const { products, cartState, favoriteState, getDataAsync } = props;

  const { toggleView, setToggleView } = useContext(ToggleViewContext);
  const classView = toggleView ? "list" : "grid";

  const dispatch = useDispatch();

  useEffect(() => {
    getDataAsync();
    const savedView = JSON.parse(localStorage.getItem("ViewContext"));
    if (savedView !== null) {
      setToggleView(savedView);
    }
  }, [setToggleView, getDataAsync]);

  useEffect(() => {
    localStorage.setItem("cartState", JSON.stringify(cartState));
    localStorage.setItem("favoriteState", JSON.stringify(favoriteState));
    localStorage.setItem("ViewContext", JSON.stringify(toggleView));
  }, [cartState, favoriteState, toggleView]);

  const handleToggleView = (e) => {
    setToggleView(!toggleView);
  };

  const handleAddCart = (product) => {
    dispatch(addToCart(product));
  };

  const handleAddFavorite = (product) => {
    if (favoriteState.counterFav.includes(product)) {
      dispatch(removeFromFavorite(product));
    } else {
      dispatch(addToFavorite(product));
    }
  };

  return products ? (
    <div className="product__board">
      <div className="product__board-items">
        <h3>Product list:</h3>
        <div className="list">
          <button
            style={{ backgroundColor: `${!toggleView ? "#bebebe" : ""}` }}
            className="button__list"
            onClick={handleToggleView}
          >
            <i className="fa fa-th-large"></i> Grid
          </button>
          <button
            style={{ backgroundColor: `${toggleView ? "#bebebe" : ""}` }}
            className="button__list"
            onClick={handleToggleView}
          >
            <i className="fa fa-bars"></i> List
          </button>
        </div>
      </div>
      <div className={`product__list-${classView}`}>
        {products.map((product) => (
          <ProductCard
            key={product.id}
            {...product}
            handleAddCart={() => handleAddCart(product)}
            handleAddFavorite={() => handleAddFavorite(product)}
            favoriteStatus={
              favoriteState.favoriteButtonColor[product.id] || "#fff"
            }
            buttonText={cartState.buttonText[product.id] || "Add to cart"}
          />
        ))}
      </div>
    </div>
  ) : (
    <Loader />
  );
}

const mapDispatchToProps = {
  getDataAsync,
};
const mapStateToProps = (state) => ({
  products: state.data.products,
  cartState: state.cart.cartItems,
  favoriteState: state.favorite.favoriteItems,
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);

Home.propTypes = {
  handleAddCart: PropTypes.func.isRequired,
  handleAddFavorite: PropTypes.func.isRequired,
};

Home.defaultProps = {
  handleAddCart: () => {},
  handleAddFavorite: () => {},
  cart: {
    buttonText: {},
  },
  favorite: {
    favoriteButtonColor: {},
  },
};
