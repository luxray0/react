import { Link } from "react-router-dom";

function Success() {
  return (
    <>
      <div className="success">
        <h2>Thank you for your purchase!</h2>
        <p>
          Your order number is:<span>1</span>
        </p>
        <p>
          We'll email you an order confirmation with details and tracking info.
        </p>
        <Link to={"/"}>
          <button className="success__back-btn"> Continue Shopping</button>
        </Link>
      </div>
    </>
  );
}

export default Success;
