import { useState } from "react";
import { ToggleViewContext } from "./contexts";

export const ToggleViewList = ({ children }) => {
  const [toggleView, setToggleView] = useState(false);

  return (
    <ToggleViewContext.Provider value={{ toggleView, setToggleView }}>
      {children}
    </ToggleViewContext.Provider>
  );
};
