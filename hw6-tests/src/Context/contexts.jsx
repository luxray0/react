import { createContext } from "react";

export const ToggleViewContext = createContext(null);
