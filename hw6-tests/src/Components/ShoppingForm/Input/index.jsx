import { useState } from "react";

function Input(props) {
  const {
    formErrors,
    inputName,
    inputPlaceholder,
    inputValue,
    onChange,
    touched,
  } = props;

  return (
    <>
      <input
        name={inputName}
        placeholder={inputPlaceholder}
        className="input"
        type="text"
        value={inputValue}
        onChange={onChange}
        onFocus={onChange}
      />
      <label htmlFor={inputName} className="error">
        {touched && formErrors[inputName] ? (
          <span>{formErrors[inputName]}</span>
        ) : (
          ""
        )}
      </label>
    </>
  );
}

export default Input;
