import PropTypes from "prop-types";
import { ToggleViewContext } from "../../../Context/contexts";
import { useContext } from "react";
function Button(props) {
  const {
    backgroundColor,
    onClick,
    className,
    text,
    onMouseEnter,
    onMouseLeave,
  } = props;

  const { toggleView } = useContext(ToggleViewContext);
  const classView = toggleView ? "animated-check-list" : "animated-check";

  return (
    <>
      <button
        style={{ backgroundColor }}
        className={`button ${className}`}
        onClick={onClick}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        disabled={text === "Added!" ? "disable" : ""}
        data-testid="button"
      >
        {text}
      </button>
      <svg
        data-testid="checkmark-svg"
        style={{ display: `${text === "Added!" ? "flex" : "none"}` }}
        className={classView}
        viewBox="0 0 24 24"
      >
        <path d="M4.1 12.7L9 17.6 20.3 6.3" fill="none" />
      </svg>
    </>
  );
}

export default Button;

Button.propTypes = {
  backgroundColor: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
  text: PropTypes.string,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
};

Button.defaultProps = {
  className: "",
  onMouseEnter: () => {},
  onMouseLeave: () => {},
};
