import React from "react";
import PropTypes from "prop-types";

function Modal({ header, closeButton, text, actions, className, onClose }) {
  const handleModalClick = (e) => {
    e.stopPropagation();
  };

  return (
    <div className={`modal ${className}`} onClick={onClose}>
      <div
        className="modal__content"
        data-testid="modal"
        onClick={handleModalClick}
      >
        <div className="modal__header">
          <h2 data-testid="header" className="modal__header">
            {header}
          </h2>
          {closeButton && (
            <button
              className="modal__content-close"
              data-testid="closeButton"
              onClick={onClose}
            >
              &times;
            </button>
          )}
        </div>
        <div data-testid="content" className="modal__content-text">
          <p>{text}</p>
        </div>
        <div data-testid="actions" className="modal__content-actions">
          {actions}
        </div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string,
  closeButton: PropTypes.bool,
  text: PropTypes.string,
  actions: PropTypes.node,
  className: PropTypes.string,
  onClose: PropTypes.func,
};

Modal.defaultProps = {
  closeButton: true,
  className: "",
};

export default Modal;
