import { useState } from 'react';

function useShoppingCart() {
    const [cartState, setCartState] = useState(JSON.parse(localStorage.getItem("cartState")) || { counterItems: [], buttonText: {} });

    const addCartItem = (productId, stateProperty) => {
        setCartState(prevState => {
            const {
                counterItems,
                buttonText,
            } = prevState;

            const updatedCounterItems =
                stateProperty === "counterItems"
                    ? [...counterItems, productId]
                    : counterItems;

            const updatedButtonText = {
                ...buttonText,
                [productId.id]: "Added!"
            };

            const updatedCartState = {
                counterItems: updatedCounterItems,
                buttonText: updatedButtonText,
            };


            return { ...prevState, ...updatedCartState };

        });
    };

    const removeCartItem = (productId, stateProperty) => {
        setCartState(prevState => {
            const updatedCounterItems =
                stateProperty === "counterItems"
                    ? prevState.counterItems.filter((id) => id !== productId)
                    : prevState.counterItems;
            const updatedButtonText = {
                ...prevState.buttonText,
                [productId.id]: "Add to cart"
            };

            const updatedCartState = {
                counterItems: updatedCounterItems,
                buttonText: updatedButtonText,
            }

            return { ...prevState, ...updatedCartState };
        });
    };


    return { cartState, addCartItem, removeCartItem };
}

export { useShoppingCart }