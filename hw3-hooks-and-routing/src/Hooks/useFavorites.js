import { useState } from 'react'

function useFavorites() {
    const [favoriteState, setFavoriteState] = useState(JSON.parse(localStorage.getItem("favoriteState")) || { counterFav: [], favoriteButtonColor: {} })

    const addFavorite = (productId, stateProperty) => {
        setFavoriteState((prevState) => {
            const { counterFav, favoriteButtonColor } = prevState

            const updatedCounterFav =
                stateProperty === 'counterFav' ? [...counterFav, productId] : counterFav

            const updatedFavoriteButtonColor = {
                ...favoriteButtonColor,
                [productId.id]: '#6ba6ff',
            }

            const updatedFavoritesState = {
                counterFav: updatedCounterFav,
                favoriteButtonColor: updatedFavoriteButtonColor,
            }

            return { ...prevState, ...updatedFavoritesState }
        })
    }

    const removeFavorite = (productId, stateProperty) => {
        setFavoriteState((prevState) => {
            const updatedCounterFav =
                stateProperty === 'counterFav'
                    ? prevState.counterFav.filter((id) => id !== productId)
                    : prevState.counterFav
            const updatedFavoriteButtonColor = {
                ...prevState.favoriteButtonColor,
                [productId.id]: '#fff',
            }
            const updatedFavoritesState = {
                counterFav: updatedCounterFav,
                favoriteButtonColor: updatedFavoriteButtonColor,
            }

            return { ...prevState, ...updatedFavoritesState }
        })
    }
    return { favoriteState, addFavorite, removeFavorite }
}

export { useFavorites }
