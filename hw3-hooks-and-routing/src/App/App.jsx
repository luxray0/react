import PropTypes from "prop-types";

import Header from "../Components/Header";
import Cart from "../Pages/Cart";
import Favorites from "../Pages/Favorites";
import Home from "../Pages/Home";

import { useState, useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import { useShoppingCart } from "../Hooks/useCart";
import { useFavorites } from "../Hooks/useFavorites";

import "../index.scss";

function App() {
  const [products, setProducts] = useState([]);
  const [cart, setCart] = useState({});
  const [favorite, setFavorite] = useState({});

  const { addFavorite, removeFavorite, favoriteState } = useFavorites();
  const { addCartItem, removeCartItem, cartState } = useShoppingCart();

  useEffect(() => {
    if (cartState) setCart((prevState) => ({ ...prevState, ...cartState }));
    if (favoriteState)
      setFavorite((prevState) => ({ ...prevState, ...favoriteState }));
  }, [cartState, favoriteState]);

  useEffect(() => {
    localStorage.setItem("cartState", JSON.stringify(cart));
    localStorage.setItem("favoriteState", JSON.stringify(favorite));
  }, [cart, favorite]);

  const handleAddFavorite = (productId) => {
    if (favorite.counterFav.includes(productId)) {
      removeFavorite(productId, "counterFav");
    } else {
      addFavorite(productId, "counterFav");
    }
  };

  const handleAddCart = (productId) => {
    if (cart.counterItems.includes(productId)) {
      removeCartItem(productId, "counterItems");
    } else {
      addCartItem(productId, "counterItems");
    }
  };

  useEffect(() => {
    fetch("./data/products.json")
      .then((response) => response.json())
      .then((data) => {
        setProducts(data.products);
      })
      .catch((error) => {
        console.log("Error>> ", error);
      });
  }, []);

  return (
    <div>
      <Header favorite={favorite} cart={cart} />
      <div className="container">
        <Routes>
          <Route
            path="/"
            element={
              <Home
                products={products}
                cart={cart}
                favorite={favorite}
                handleAddFavorite={handleAddFavorite}
                handleAddCart={handleAddCart}
              />
            }
          />
          <Route
            path="/Cart"
            element={<Cart cart={cart} removeCartItem={removeCartItem} />}
          />
          <Route
            path="/Favorites"
            element={
              <Favorites favorite={favorite} removeFavorite={removeFavorite} />
            }
          />
          <Route path="/*" element={<h2>Not found</h2>} />
        </Routes>
      </div>
    </div>
  );
}

export default App;

App.propTypes = {
  products: PropTypes.array.isRequired,
  cart: PropTypes.object.isRequired,
  favorite: PropTypes.object.isRequired,
  handleAddCart: PropTypes.func.isRequired,
  handleAddFavorite: PropTypes.func.isRequired,
  removeCartItem: PropTypes.func.isRequired,
  removeFavorite: PropTypes.func.isRequired,
};

App.defaultProps = {
  products: [],
  cart: {},
  favorite: {},
  handleAddCart: () => {},
  handleAddFavorite: () => {},
  removeCartItem: () => {},
  removeFavorite: () => {},
};
