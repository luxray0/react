import React from 'react';
import PropTypes from 'prop-types';

function Modal({ header, closeButton, text, actions, className, onClose }) {
  const handleModalClick = (e) => {
    e.stopPropagation();
  };

  return (
    <div className={`modal ${className}`} onClick={onClose}>
      <div className='modal__content' onClick={handleModalClick}>
        <div className='modal__header'>
          <h2 className='modal__header'>{header}</h2>
          {closeButton && (
            <button className='modal__content-close' onClick={onClose}>
              &times;
            </button>
          )}
        </div>
        <div className='modal__content-text'>
          <p>{text}</p>
        </div>
        <div className='modal__content-actions'>{actions}</div>
      </div>
    </div>
  );
}

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
  actions: PropTypes.node.isRequired,
  className: PropTypes.string,
  onClose: PropTypes.func.isRequired,
};

Modal.defaultProps = {
  closeButton: true,
  className: '',
};

export default Modal;