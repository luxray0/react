import React, { useState } from "react";
import Button from "./Button/Button";
import Modal from "./Modal/Modal";
import PropTypes from "prop-types";
import modals from "../../Data/Data";

function ConfirmModal(props) {
  const [modalOpen, setModalOpen] = useState(false);
  const [modalIndex, setModalIndex] = useState(0);
  const [toggleBackground, setToggleBackground] = useState(false);

  const openModal = (index) => {
    setModalOpen(true);
    setModalIndex(index);
  };

  const closeModal = () => {
    setModalOpen(false);
  };

  const changeBackground = (modalIndex) => {
    setToggleBackground(!toggleBackground);
  };

  const confirmAndCloseModal = () => {
    props.confirm();
    closeModal();
  };

  const modal = modals[modalIndex] || {};

  return (
    <>
      <Button
        text={props.buttonText}
        className={`${props.buttonClass} ${
          toggleBackground ? "btn__hover" : ""
        }`}
        backgroundColor={modal.backgroundColor}
        onClick={() => openModal(props.index)}
        onMouseEnter={changeBackground}
        onMouseLeave={changeBackground}
      />
      {modalOpen && modal && (
        <Modal
          className={`${modal.modalClass}`}
          header={modal.title}
          closeButton={modal.closeButton}
          text={modal.description}
          actions={
            <>
              <button
                className={`modal__content-btn ${modal.modalClass}`}
                onClick={confirmAndCloseModal}
              >
                {modal.confirm}
              </button>
              <button
                className={`modal__content-btn ${modal.modalClass}`}
                onClick={closeModal}
              >
                Cancel
              </button>
            </>
          }
          onClose={closeModal}
        />
      )}
    </>
  );
}

ConfirmModal.propTypes = {
  buttonText: PropTypes.string,
  buttonClass: PropTypes.string,
  index: PropTypes.number.isRequired,
  confirm: PropTypes.func.isRequired,
};

ConfirmModal.defaultProps = {
  buttonText: "Confirm",
  buttonClass: "",
};

export default ConfirmModal;
