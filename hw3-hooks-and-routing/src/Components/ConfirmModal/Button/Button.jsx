import PropTypes from "prop-types";

function Button(props) {
  const {
    backgroundColor,
    onClick,
    className,
    text,
    onMouseEnter,
    onMouseLeave,
  } = props;

  return (
    <>
      <button
        style={{ backgroundColor }}
        className={`button ${className}`}
        onClick={onClick}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        disabled={text === "Added!" ? "disable" : ""}
      >
        {text}
      </button>
      <svg
        style={{ display: `${text === "Added!" ? "flex" : "none"}` }}
        className="animated-check"
        viewBox="0 0 24 24"
      >
        <path d="M4.1 12.7L9 17.6 20.3 6.3" fill="none" />
      </svg>
    </>
  );
}

export default Button;

Button.propTypes = {
  backgroundColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  text: PropTypes.string.isRequired,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
};

Button.defaultProps = {
  className: "",
  onMouseEnter: () => {},
  onMouseLeave: () => {},
};
