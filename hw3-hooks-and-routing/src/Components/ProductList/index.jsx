import ProductCard from "../ProductCard";
import PropTypes from "prop-types";

function ProductList(props) {
  const { products, handleAddCart, handleAddFavorite, cart, favorite } = props;

  return products ? (
    <div className="product__board">
      <h3>Product list:</h3>
      <div className="product__list">
        {products.map((product) => (
          <ProductCard
            key={product.id}
            {...product}
            handleAddCart={() => handleAddCart(product)}
            handleAddFavorite={() => handleAddFavorite(product)}
            favoriteStatus={favorite.favoriteButtonColor[product.id] || "#fff"}
            buttonText={cart.buttonText[product.id] || "Add to cart"}
          />
        ))}
      </div>
    </div>
  ) : (
    <h1>Loading...</h1>
  );
}
export default ProductList;

ProductList.propTypes = {
  handleAddCart: PropTypes.func.isRequired,
  handleAddFavorite: PropTypes.func.isRequired,
};

ProductList.defaultProps = {
  products: null,
  handleAddCart: () => {},
  handleAddFavorite: () => {},
  cart: {
    buttonText: {},
  },
  favorite: {
    favoriteButtonColor: {},
  },
};
