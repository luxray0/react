import ProductList from "../../Components/ProductList";
import PropTypes from "prop-types";

function Home(props) {
  const { products, cart, favorite, handleAddFavorite, handleAddCart } = props;

  return (
    <ProductList
      products={products}
      cart={cart}
      favorite={favorite}
      handleAddFavorite={handleAddFavorite}
      handleAddCart={handleAddCart}
    />
  );
}

export default Home;

Home.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      article: PropTypes.string.isRequired,
      url: PropTypes.string.isRequired,
      color: PropTypes.string.isRequired,
      price: PropTypes.string.isRequired,
    })
  ).isRequired,
  handleAddFavorite: PropTypes.func.isRequired,
  handleAddCart: PropTypes.func.isRequired,
};

Home.defaultProps = {
  products: [],
  cart: { counterItems: [], totalPrice: 0 },
  favorite: { counterFav: [] },
  handleAddFavorite: () => {},
  handleAddCart: () => {},
};
