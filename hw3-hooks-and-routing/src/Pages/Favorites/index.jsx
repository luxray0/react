import PropTypes from "prop-types";
import ProductCard from "../../Components/ProductCard";
import { Link } from "react-router-dom";

function Favorites(props) {
  const { favorite, removeFavorite } = props;

  return (
    <>
      {favorite.counterFav?.length ? (
        <div className="favs">
          <h2 className="favs__title">Favorites</h2>
          <div className="favs__wrapper">
            {favorite.counterFav?.map((product) => (
              <div key={product.id} className="favs__card">
                <ProductCard
                  name={product.name}
                  article={product.article}
                  url={product.url}
                  color={product.color}
                  price={product.price}
                  favoriteStatus="#6ba6ff"
                  handleAddFavorite={() =>
                    removeFavorite(product, "counterFav")
                  }
                  buttonClass={"none"}
                />
              </div>
            ))}
          </div>
          <Link to={"/"}>
            <button className="back__btn"> Continue Shopping</button>
          </Link>
        </div>
      ) : (
        <h2>No products have been added yet</h2>
      )}
    </>
  );
}
export default Favorites;

Favorites.propTypes = {
  favorite: PropTypes.shape({
    counterFav: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        article: PropTypes.string.isRequired,
        url: PropTypes.string.isRequired,
        color: PropTypes.string.isRequired,
        price: PropTypes.string.isRequired,
      })
    ),
  }).isRequired,
  removeFavorite: PropTypes.func.isRequired,
};

Favorites.defaultProps = {
  favorite: {
    counterFav: [],
  },
  removeFavorite: () => {},
};
