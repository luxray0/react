const modals = [
    {
        id: 1,
        title: 'Confirmation',
        description: 'Do you want to add this product to your cart?',
        closeButton: true,
        confirm: 'Add',
        backgroundColor: "#0068bf",
        buttonText: 'Add to cart',
        modalClass: 'second'
    },
    {
        id: 2,
        title: 'Confirmation',
        description: 'Do you want to remove this product from your cart?',
        closeButton: true,
        confirm: 'Remove',
        backgroundColor: "#e45d85",
        buttonText: 'Remove from cart',
        modalClass: 'first'
    },

];




export default modals
