import { addCartItem, removeCartItem } from "../types";

const initialState = {
    cartItems: {
        counterItems: JSON.parse(localStorage.getItem("cartState"))?.counterItems || [],
        buttonText: JSON.parse(localStorage.getItem("cartState"))?.buttonText || {},
    },
};

export const cartReducer = (state = initialState, action) => {
    switch (action.type) {
        case addCartItem.ADD_TO_CART:

            return {
                ...state,
                cartItems: {
                    counterItems: [...state.cartItems.counterItems, action.payload.counterItems],
                    buttonText: {
                        ...state.cartItems.buttonText,
                        [action.payload.counterItems.id]: "Added!",
                    },
                },
            };

        case removeCartItem.REMOVE_FROM_CART:
            return {
                ...state,
                cartItems: {
                    counterItems: state.cartItems.counterItems.filter((item) => item.id !== action.payload.counterItems.id),
                    buttonText: {
                        ...state.cartItems.buttonText,
                        [action.payload.counterItems.id]: "Add to cart",
                    },
                },
            };
        default:

            return state;

    }
};
