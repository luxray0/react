export const dataTypes = {
    SET_PRODUCTS: "Data - SET_PRODUCTS",
}
export const addCartItem = {
    ADD_TO_CART: "Cart - ADD_TO_CART",
}
export const removeCartItem = {
    REMOVE_FROM_CART: "Cart - REMOVE_FROM_CART",
}
export const addFavoriteItem = {
    ADD_TO_FAVORITE: "Favorite - ADD_TO_FAVORITE",
}
export const removeFavoriteItem = {
    REMOVE_FROM_FAVORITE: "Favorite - REMOVE_FROM_FAVORITE",
}
export const isModalOpen = {
    SET_MODAL_OPEN: "Modal - SET_MODAL_OPEN",
}