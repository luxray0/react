import { addFavoriteItem, removeFavoriteItem } from "../types";

export const addToFavorite = (product) => {
    return (dispatch) => {
        dispatch({
            type: addFavoriteItem.ADD_TO_FAVORITE,
            payload: {
                counterFav: product,
                favoriteButtonColor: {}
            },
        });
    };
};

export const removeFromFavorite = (product) => {
    return (dispatch) => {
        dispatch({
            type: removeFavoriteItem.REMOVE_FROM_FAVORITE,
            payload: {
                counterFav: product,
                favoriteButtonColor: {}
            },
        });
    };
};