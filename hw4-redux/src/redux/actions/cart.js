import { addCartItem, removeCartItem } from "../types";

export const addToCart = (product) => {
    return (dispatch) => {
        dispatch({
            type: addCartItem.ADD_TO_CART,
            payload: {
                counterItems: product,
                buttonText: {},
            },
        });
    };
};

export const removeFromCart = (product) => {
    return (dispatch) => {
        dispatch({
            type: removeCartItem.REMOVE_FROM_CART,
            payload: {
                counterItems: product,
                buttonText: {},
            },
        });
    };
};