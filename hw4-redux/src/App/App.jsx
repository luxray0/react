import Header from "../Components/Header";
import Cart from "../Pages/Cart";
import Favorites from "../Pages/Favorites";
import Home from "../Pages/Home";

import { Route, Routes } from "react-router-dom";

import "../index.scss";

function App() {
  return (
    <>
      <Header />
      <main className="container">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/Cart" element={<Cart />} />
          <Route path="/Favorites" element={<Favorites />} />
          <Route path="/*" element={<h2>Not found</h2>} />
        </Routes>
      </main>
    </>
  );
}

export default App;
