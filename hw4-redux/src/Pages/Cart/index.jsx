import PropTypes from "prop-types";
import ConfirmModal from "../../Components/ConfirmModal";
import Loader from "../../Components/Loaders";

import { useEffect } from "react";
import { Link } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { removeFromCart } from "../../redux/actions/cart";
function Cart(props) {
  const { cartState } = props;

  const dispatch = useDispatch();

  useEffect(() => {
    localStorage.setItem("cartState", JSON.stringify(cartState));
  }, [cartState]);

  return (
    <>
      {cartState.counterItems?.length ? (
        <div className="cart">
          <div className="cart__titles">
            <h2>Shopping cart</h2>
            <h3>{cartState.counterItems?.length} Items</h3>
          </div>
          <div className="cart__columns">
            <p>Product</p>
            <p>Quantity</p>
            <p>Price</p>
            <p>Total</p>
          </div>
          <div className="cart__wrapper">
            {cartState.counterItems.map((product) => (
              <div key={product.id} className="cart__wrapper-item">
                <img src={product.url} alt={product.name} />
                <p className="item__name">{product.name}</p>
                <div className="cart__quantity">
                  <button className="cart__quantity-btn">-</button>
                  <div className="cart__quantity-count">1</div>
                  <button className="cart__quantity-btn">+</button>
                </div>
                <p className="item__price">{product.price} $</p>
                <p className="item__price">{product.price} $</p>
                <ConfirmModal
                  key={product.id}
                  buttonClass="remove__item"
                  buttonText=""
                  index={1}
                  confirm={() => dispatch(removeFromCart(product))}
                />
              </div>
            ))}
          </div>
          <Link to={"/"}>
            <button className="back__btn"> Continue Shopping</button>
          </Link>
        </div>
      ) : (
        <>
          <h2>No products have been added yet</h2>
          <Loader />
        </>
      )}
    </>
  );
}

const mapStateToProps = (state) => ({
  cartState: state.cart.cartItems,
});

export default connect(mapStateToProps)(Cart);

Cart.propTypes = {
  cartState: PropTypes.shape({}).isRequired,
};

Cart.defaultProps = {
  cartState: {
    counterItems: [],
  },
};
