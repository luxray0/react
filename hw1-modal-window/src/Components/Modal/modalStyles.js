const styles = {
    modal: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'fixed',
        color: '#fff',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        transition: ".2s",
        overflow: "auto",
    },
    modalHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        backgroundColor: '#d54430',
        padding: [20, 10, 20, 20],
        '&.second': {
            backgroundColor: "#2d37a0"
        },
    },
    modalHeaderTitle: {
        fontWeight: 600
    },
    modalContent: {
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: "#ea4a34",
        borderRadius: 10,
        overflow: "hidden",
        '&.second': {
            backgroundColor: "#3953d7"
        },
    },
    modalContentClose: {
        cursor: 'pointer',
        transform: 'scale(2)',
        background: 'none',
        color: "#fff"
    },
    modalContentText: {
        textAlign: 'center',
        fontSize: 16,
        lineHeight: '40px',
        marginTop: 40
    },
    modalContentGraph: {
        padding: {
            left: 50,
            right: 50
        }
    },
    modalContentActions: {
        display: 'flex',
        justifyContent: 'center',
        margin: {
            top: 20,
            bottom: 20
        }
    }
}

export { styles }