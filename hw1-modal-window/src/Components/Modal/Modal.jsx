import React, { Component } from 'react';
import withStyles from 'react-jss'
import { styles } from './modalStyles';

class Modal extends Component {
   
    render() {
        const { header, closeButton, text, actions, className, classes } = this.props;

        return (
            <div className={`${classes.modal}`} onClick={this.props.onClose}>
                <div className={`${classes.modalContent} ${className}`}onClick={e => e.stopPropagation()}>
                    <div className={`${classes.modalHeader} ${className}`}>
                        <h2 className={classes.modalHeaderTitle}>{header}</h2>
                        {closeButton && (
                            <button className={classes.modalContentClose} onClick={this.props.onClose}>
                                &times;
                            </button>
                        )}
                    </div>
                    <div className={classes.modalContentText}>
                        <p className={classes.modalContentGraph}>{text}</p>
                    </div>
                    <div className={classes.modalContentActions}>{actions}</div>
                </div>
            </div>
        );
    }
}

const StyledModal = withStyles(styles)(Modal)

export default StyledModal