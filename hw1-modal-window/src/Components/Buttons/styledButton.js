const styles = {
    button: {
        color: '#fff',
        cursor: 'pointer',
        margin: 20,
        padding: [17, 40],
        borderRadius: 12,
        letterSpacing: 1.5,
        textTransform: 'uppercase',
        fontSize: 15,
        outline: 'none',
        transition: 'all .3s ease',
        fontWeight: 500,
        boxShadow: "transparent 0 0 0 3px,rgba(18, 18, 18, .1) 0 6px 20px",
        '&:hover': {
            backgroundColor: '#dddddd !important',
            color: '#000',
            fontWeight: 600,
            boxShadow: '#505050 0 0 0 3px, transparent 0 0 0 0'
        },
        '&:active': {
            transform: "scale(0.93)",
        }
    }
}

export { styles };