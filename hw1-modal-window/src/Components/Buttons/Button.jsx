import React, { Component } from 'react';
import withStyles from 'react-jss'
import { styles } from './styledButton';


class Button extends Component {
    render() {
        const { backgroundColor, text, onClick, classes } = this.props;
        
        return (
            <button style={{ backgroundColor }} className={classes.button} onClick={onClick}>
                <span>{text}</span>
            </button>
        );
    }
}
const StyledButton = withStyles(styles)(Button)

export default StyledButton

