const styles = {
    container: {
        display: 'flex',
        justifyContent: "center",
        alignItems: "center",
        fontFamily: 'Montserrat, sans-serif',
        minHeight: "100vh",
        backgroundColor: "#dddddd",
        transition: "all .3s ease"
    },
    modalContentBtn: {
        backgroundColor: "#b53125",
        '&:hover': {
            backgroundColor: "#842d21"
        },
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 7,
        padding: {
            top: 10,
            bottom: 10
        },
        minWidth: 100,
        color: '#fff',
        borderRadius: 5,
        cursor: "pointer",
        transition: "all .2s ease",
        '&.secondButton': {
            backgroundColor: "#29327f82",
            '&:hover': {
                backgroundColor: "#29327f"
            },
        },
    },
}

export { styles }