import React, { Component } from 'react';
import withStyles from 'react-jss';
import { styles } from './appStyles';
import Modal from '../Components/Modal/Modal';
import Button from '../Components/Buttons/Button';
import modals from '../Data/Data';

import '../index.scss'

class App extends Component {

  state = {
    modalOpen: null
  };

  openModal = (id) => {
    this.setState({ modalOpen: id });
  };

  closeModal = () => {
    this.setState({ modalOpen: null });
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.container}>
        {modals.map(modal => (
          <React.Fragment key={modal.id}>
            <Button
              backgroundColor={modal.backgroundColor}
              text={`Open ${modal.buttonText} modal`}
              onClick={() => this.openModal(modal.id)}
            />
            {this.state.modalOpen === modal.id && (
              <Modal
                className={modal.buttonText}
                header={modal.title}
                closeButton={modal.closeButton}
                text={modal.description}
                actions= {
                  <>
                      <button className={`${classes.modalContentBtn} ${modal.buttonClass}`} onClick={this.closeModal}>{modal.confirm}</button>
                      <button className={`${classes.modalContentBtn} ${modal.buttonClass}`} onClick={this.closeModal}>Cancel</button>
                  </>
              }
                onClose={this.closeModal}
              />
            )}
          </React.Fragment>
        ))}
      </div>
    );
  }
}


const StyledApp = withStyles(styles)(App)

export default StyledApp