import { dataTypes } from "../types";
import axios from 'axios';

export const setData = (data) => {
    return {
        type: dataTypes.SET_PRODUCTS,
        payload: data,
    };
}

export const getDataAsync = () => {
    return async function (dispatch) {
        try {
            const response = await axios.get('./data/products.json');
            const results = response.data;
            dispatch(setData(results));
        } catch (err) {
            console.error('Error fetching data:', err);
        }
    };
}