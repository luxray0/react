import { isModalOpen } from "../types";

const initialState = {
    isModalOpen: false
};

export const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case isModalOpen.SET_MODAL_OPEN:
            return {
                ...state,
                isModalOpen: action.payload
            };
        default:
            return state;
    }
};