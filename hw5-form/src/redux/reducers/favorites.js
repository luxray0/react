import { addFavoriteItem, removeFavoriteItem } from "../types";

const initialState = {
    favoriteItems: {
        counterFav: JSON.parse(localStorage.getItem("favoriteState"))?.counterFav || [],
        favoriteButtonColor: JSON.parse(localStorage.getItem("favoriteState"))?.favoriteButtonColor || {}
    },
};

export const favoriteReducer = (state = initialState, action) => {
    switch (action.type) {
        case addFavoriteItem.ADD_TO_FAVORITE:

            return {
                ...state,
                favoriteItems: {
                    counterFav: [...state.favoriteItems.counterFav, action.payload.counterFav],
                    favoriteButtonColor: {
                        ...state.favoriteItems.favoriteButtonColor,
                        [action.payload.counterFav.id]: "#6ba6ff",
                    },
                }


            };
        case removeFavoriteItem.REMOVE_FROM_FAVORITE:
            return {
                ...state,
                favoriteItems: {
                    counterFav: state.favoriteItems.counterFav.filter((item) => item.id !== action.payload.counterFav.id),
                    favoriteButtonColor: {
                        ...state.favoriteItems.favoriteButtonColor,
                        [action.payload.counterFav.id]: "#fff",
                    },
                }


            };
        default:
            return state;
    }
};

