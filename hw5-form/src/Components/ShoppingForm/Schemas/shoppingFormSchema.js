import * as Yup from "yup";

export const validationSchema = Yup.object({
    firstName: Yup.string()
        .matches(/^[а-яА-ЯёЁa-zA-Z]+$/, 'Only letters are allowed')
        .min(2, "Min 2 letters")
        .max(15, "Max 15 letters")
        .required("This field is required"),
    lastName: Yup.string()
        .matches(/^[а-яА-ЯёЁa-zA-Z]+$/, 'Only letters are allowed')
        .min(2, "Min 2 letters")
        .max(20, "Max 20 letters")
        .required("This field is required"),
    age: Yup.number()
        .min(18, 'Must be at least 18 years old')
        .max(99, "Age must be between 18 and 99 years old")
        .typeError('Only numbers are allowed')
        .required("This field is required"),
    address: Yup.string()
        .required("This field is required")
        .min(10, "Min 10 symbols")
        .max(40, "Max 40 symbols"),
    phone: Yup.string()
        .min(14, "Min 14 symbols")
        .required("This field is required"),
});
