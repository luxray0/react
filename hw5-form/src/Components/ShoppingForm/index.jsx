import { useNavigate } from "react-router-dom";
import { connect, useDispatch } from "react-redux";
import { clearCartAction } from "../../redux/actions/cart";

import { useFormik } from "formik";
import { validationSchema } from "./Schemas/shoppingFormSchema";
import { PatternFormat } from "react-number-format";

import Input from "./Input";

function ShoppingForm(props) {
  const { cartState } = props;

  const navigate = useNavigate();

  const productPrices = cartState.counterItems.map((item) =>
    parseFloat(item.price)
  );
  const productPricesSum = productPrices.reduce(
    (accumulator, currentValue) => accumulator + currentValue,
    0
  );

  const dispatch = useDispatch();

  const form = useFormik({
    initialValues: {
      firstName: "",
      lastName: "",
      age: "",
      phone: "",
      address: "",
    },
    validationSchema,
    onSubmit: (values) => {
      console.log(cartState.counterItems);
      console.log(values);
      dispatch(clearCartAction());
      navigate("/Success");
    },
  });

  return (
    <>
      <div className="shopping__cart-form">
        <h2>Order summary</h2>
        <p>For orders please fill up the form</p>
        <form className="shopping__form" onSubmit={form.handleSubmit}>
          <Input
            formErrors={form.errors}
            inputName="firstName"
            inputPlaceholder="First name"
            inputValue={form.values.firstName}
            onChange={form.handleChange}
            touched={form.touched.firstName}
          />
          <Input
            formErrors={form.errors}
            inputName="lastName"
            inputPlaceholder="Last name"
            inputValue={form.values.lastName}
            onChange={form.handleChange}
            touched={form.touched.lastName}
          />
          <br />
          <Input
            formErrors={form.errors}
            inputName="age"
            inputPlaceholder="Age"
            inputValue={form.values.age}
            onChange={form.handleChange}
            touched={form.touched.age}
          />
          <Input
            formErrors={form.errors}
            inputName="address"
            inputPlaceholder="Address"
            inputValue={form.values.address}
            onChange={form.handleChange}
            touched={form.touched.address}
          />
          <PatternFormat
            name="phone"
            placeholder="Phone Number"
            className="input"
            format="(###)###-##-##"
            value={form.values.phone}
            onChange={form.handleChange}
          />
          <label htmlFor="phone" className="error">
            {form.touched.phone && form.errors.phone ? (
              <span>{form.errors.phone}</span>
            ) : (
              ""
            )}
          </label>
          <div className="shopping__form-total">
            <p>Total cost</p>
            <p>
              <strong>${productPricesSum}</strong>
            </p>
          </div>
          <button
            className="checkout__button"
            type="submit"
            onSubmit={form.handleSubmit}
          >
            Checkout
          </button>
        </form>
      </div>
    </>
  );
}
const mapStateToProps = (state) => ({
  cartState: state.cart.cartItems,
});

export default connect(mapStateToProps)(ShoppingForm);
