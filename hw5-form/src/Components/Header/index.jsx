import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
function Header(props) {
  const { favoriteState, cartState } = props;

  const isFavEmpty = favoriteState.counterFav?.length === 0;
  const isCartEmpty = cartState.counterItems?.length === 0;

  return (
    <header className="header">
      <div className="header__items">
        <Link to={"/"}>
          <img src="image/icons/main-logo.png" alt="main-logo" />
        </Link>
        <Link to={"/"}>
          <h1>Store</h1>
        </Link>
        <div className="header__icons">
          <Link to={"/cart"}>
            <img src="image/icons/cart.png" alt="cart" />
          </Link>

          <div
            style={{ display: `${isCartEmpty ? "none" : "flex"}` }}
            className="count count__items"
          >
            {cartState.counterItems?.length}
          </div>
          <Link to={"/favorites"}>
            <img src="image/icons/star1.png" alt="star" />
          </Link>

          <div
            style={{ display: `${isFavEmpty ? "none" : "flex"}` }}
            className="count count__favorites"
          >
            {favoriteState.counterFav?.length}
          </div>
        </div>
      </div>
    </header>
  );
}
const mapStateToProps = (state) => ({
  cartState: state.cart.cartItems,
  favoriteState: state.favorite.favoriteItems,
});

export default connect(mapStateToProps)(Header);

Header.propTypes = {
  favorite: PropTypes.shape({
    counterFav: PropTypes.arrayOf(PropTypes.object),
  }),
  cart: PropTypes.shape({
    counterItems: PropTypes.arrayOf(PropTypes.object),
  }),
};

Header.defaultProps = {
  favorite: {
    counterFav: [],
  },
  cart: {
    counterItems: [],
  },
};
