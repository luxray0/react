import ReactBodymovin from "react-bodymovin";

function Loader() {
  const bodymovinOptions = {
    animType: "svg",
    loop: true,
    prerender: true,
    autoplay: true,
    path: "https://s3-us-west-2.amazonaws.com/s.cdpn.io/35984/LEGO_loader.json",
  };
  return (
    <>
      <div style={{ maxWidth: "460px" }}>
        <ReactBodymovin options={bodymovinOptions} />
      </div>
    </>
  );
}

export default Loader;
