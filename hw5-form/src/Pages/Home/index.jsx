import PropTypes from "prop-types";
import ProductCard from "../../Components/ProductCard";
import Loader from "../../Components/Loaders";

import { useEffect } from "react";
import { connect, useDispatch } from "react-redux";
import { getDataAsync } from "../../redux/actions/products";
import { addToCart } from "../../redux/actions/cart";
import {
  addToFavorite,
  removeFromFavorite,
} from "../../redux/actions/favorites";

function Home(props) {
  const { products, cartState, favoriteState } = props;

  const dispatch = useDispatch();

  useEffect(() => {
    props.getDataAsync();
  }, []);

  useEffect(() => {
    localStorage.setItem("cartState", JSON.stringify(cartState));
    localStorage.setItem("favoriteState", JSON.stringify(favoriteState));
  }, [cartState, favoriteState]);

  const handleAddCart = (product) => {
    dispatch(addToCart(product));
  };

  const handleAddFavorite = (product) => {
    if (favoriteState.counterFav.includes(product)) {
      dispatch(removeFromFavorite(product));
    } else {
      dispatch(addToFavorite(product));
    }
  };

  return products ? (
    <div className="product__board">
      <h3>Product list:</h3>
      <div className="product__list">
        {products.map((product) => (
          <ProductCard
            key={product.id}
            {...product}
            handleAddCart={() => handleAddCart(product)}
            handleAddFavorite={() => handleAddFavorite(product)}
            favoriteStatus={
              favoriteState.favoriteButtonColor[product.id] || "#fff"
            }
            buttonText={cartState.buttonText[product.id] || "Add to cart"}
          />
        ))}
      </div>
    </div>
  ) : (
    <Loader />
  );
}

const mapStateToProps = (state) => ({
  products: state.data.data.products,
  cartState: state.cart.cartItems,
  favoriteState: state.favorite.favoriteItems,
});

const mapDispatchToProps = {
  getDataAsync,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

Home.propTypes = {
  handleAddCart: PropTypes.func.isRequired,
  handleAddFavorite: PropTypes.func.isRequired,
};

Home.defaultProps = {
  products: null,
  handleAddCart: () => {},
  handleAddFavorite: () => {},
  cart: {
    buttonText: {},
  },
  favorite: {
    favoriteButtonColor: {},
  },
};
