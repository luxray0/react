import React, { Component } from "react";
import Button from "./Button/Button";
import Modal from "./Modal/Modal";
import PropTypes from "prop-types";
import modals from "../../Data/Data";

class ConfirmModal extends Component {
  state = {
    modalOpen: false,
    modalIndex: 0,
    toggleBackground: false,
  };

  openModal = (index) => {
    this.setState({
      modalOpen: true,
      modalIndex: index,
    });
  };

  closeModal = () => {
    this.setState({ modalOpen: false });
  };

  changeBackground = () => {
    this.setState((prevState) => ({
      toggleBackground: !prevState.toggleBackground,
    }));
  };

  confirmAndCloseModal = () => {
    this.props.confirm();
    this.closeModal();
  };

  render() {
    const { modalOpen, modalIndex, toggleBackground } = this.state;
    const modal = modals[modalIndex];

    return (
      <>
        <Button
          text={this.props.buttonText}
          className={`show ${toggleBackground ? "btn__hover" : ""}`}
          backgroundColor={modal.backgroundColor}
          onClick={() => this.openModal(modal.id - 1)}
          onMouseEnter={this.changeBackground}
          onMouseLeave={this.changeBackground}
        />
        {modalOpen && (
          <Modal
            className={`${modal.modalClass}`}
            header={modal.title}
            closeButton={modal.closeButton}
            text={modal.description}
            actions={
              <>
                <button
                  className={`modal__content-btn ${modal.modalClass}`}
                  onClick={this.confirmAndCloseModal}
                >
                  {modal.confirm}
                </button>
                <button
                  className={`modal__content-btn ${modal.modalClass}`}
                  onClick={this.closeModal}
                >
                  Cancel
                </button>
              </>
            }
            onClose={this.closeModal}
          />
        )}
      </>
    );
  }
}
ConfirmModal.propTypes = {
  confirm: PropTypes.func.isRequired,
  firstButtonVisible: PropTypes.string,
  secondButtonVisible: PropTypes.string,
};

ConfirmModal.defaultProps = {
  firstButtonVisible: "",
  secondButtonVisible: "",
};

export default ConfirmModal;
