import React, { Component } from 'react';

class Modal extends Component {
   
    render() {
        const { header, closeButton, text, actions, className } = this.props;

        return (
            <div className={`modal ${className}`} onClick={this.props.onClose}>
                <div className='modal__content' onClick={e => e.stopPropagation()}>
                    <div className='modal__header'>
                        <h2 className="modal__header">{header}</h2>
                        {closeButton && (
                            <button className='modal__content-close' onClick={this.props.onClose}>
                                &times;
                            </button>
                        )}
                    </div>
                    <div className='modal__content-text'>
                        <p>{text}</p>
                    </div>
                    <div className="modal__content-actions">{actions}</div>
                </div>
            </div>
        );
    }
}


export default Modal