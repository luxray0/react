import React, { Component } from "react";
import PropTypes from "prop-types";

class Button extends Component {
  render() {
    const {
      backgroundColor,
      onClick,
      className,
      text,
      onMouseEnter,
      onMouseLeave,
    } = this.props;

    return (
      <button
        style={{ backgroundColor }}
        className={`button ${className}`}
        onClick={onClick}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        disabled={text === "Added!" ? "disable" : ""}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  backgroundColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  className: PropTypes.string,
  text: PropTypes.string.isRequired,
};

Button.defaultProps = {
  className: "",
};

export default Button;
