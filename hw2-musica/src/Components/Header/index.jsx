import React, { Component } from "react";

class Header extends Component {
  refreshPage() {
    window.location.reload(false);
  }
  render() {
    return (
      <>
        <header className="header">
          <div className="header__items">
            <img
              onClick={this.refreshPage}
              src="image/icons/main-logo.png"
              alt="main-logo"
            />
            <h1 onClick={this.refreshPage}>Store</h1>
            <div className="header__icons">
              <img src="image/icons/cart.png" alt="cart" />
              <div
                style={{ display: `${this.props.counterItemsVisible}` }}
                className="count count__items"
              >
                {this.props.counterItems}
              </div>
              <img src="image/icons/star1.png" alt="star" />
              <div
                style={{ display: `${this.props.counterFavVisible}` }}
                className="count count__favorites"
              >
                {this.props.counterFav}
              </div>
            </div>
          </div>
        </header>
      </>
    );
  }
}

export default Header;
