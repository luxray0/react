import React, { Component } from "react";
import Header from "../Components/Header";
import ProductList from "../Pages/ProductList";
import PropTypes from "prop-types";
import { addCart, removeCart } from "../Pages/Cart";
import { addFavorite, removeFavorite } from "../Pages/Favorites";

import "../index.scss";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      counterFavVisible: "none",
      counterItemsVisible: "none",
      favoriteState: {
        favoriteButtonColor: {},
        counterFav: [],
      },
      cartState: {
        buttonText: "",
        counterItems: [],
      },
    };

    this.addFavorite = addFavorite.bind(this);
    this.removeFavorite = removeFavorite.bind(this);

    this.addCart = addCart.bind(this);
    this.removeCart = removeCart.bind(this);
  }

  componentDidMount = () => {
    try {
      const cartState = JSON.parse(localStorage.getItem("cartState")) || {
        buttonText: "",
        counterItems: [],
      };
      const favoriteState = JSON.parse(
        localStorage.getItem("favoriteState")
      ) || {
        favoriteButtonColor: {},
        counterFav: [],
      };

      this.setState({
        counterFavVisible:
          favoriteState.counterFav.length > 0 ? "flex" : "none",
        counterItemsVisible:
          cartState.counterItems.length > 0 ? "flex" : "none",
        cartState: {
          counterItems: cartState.counterItems,
          buttonText: cartState.buttonText,
        },
        favoriteState: {
          favoriteButtonColor: favoriteState.favoriteButtonColor || {},
          counterFav: favoriteState.counterFav,
        },
      });
    } catch (error) {
      console.error("Error while parsing localStorage data >>", error);
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.state.cartState !== prevState.cartState) {
      localStorage.setItem("cartState", JSON.stringify(this.state.cartState));
    }
    if (this.state.favoriteState !== prevState.favoriteState) {
      localStorage.setItem(
        "favoriteState",
        JSON.stringify(this.state.favoriteState)
      );
    }
  }
  render() {
    const {
      counterFavVisible,
      counterItemsVisible,
      favoriteState,
      cartState,
    } = this.state;

    const { counterFav, favoriteButtonColor } = favoriteState;
    const { counterItems, buttonText } = cartState;

    return (
      <>
        <Header
          counterFav={counterFav.length}
          counterItems={counterItems.length}
          counterFavVisible={counterFavVisible}
          counterItemsVisible={counterItemsVisible}
        />
        <div className="container">
          <ProductList
            buttonText={buttonText}
            favoriteStatus={favoriteButtonColor}
            addToCart={(productId) => {
              if (counterItems.includes(productId)) {
                this.removeCart(productId, "counterItems");
              } else {
                this.addCart(productId, "counterItems");
              }
            }}
            addToFavorite={(productId) => {
              if (counterFav.includes(productId)) {
                this.removeFavorite(productId, "counterFav");
              } else {
                this.addFavorite(productId, "counterFav");
              }
            }}
          />
        </div>
      </>
    );
  }
}
App.propTypes = {
  counterFavVisible: PropTypes.string.isRequired,
  counterItemsVisible: PropTypes.string.isRequired,
  favoriteState: PropTypes.shape({
    favoriteButtonColor: PropTypes.object.isRequired,
    counterFav: PropTypes.array.isRequired,
  }),
  cartState: PropTypes.shape({
    buttonText: PropTypes.string.isRequired,
    counterItems: PropTypes.array.isRequired,
  }),
};

App.defaultProps = {
  counterFav: 0,
  counterItems: 0,
  counterFavVisible: "none",
  counterItemsVisible: "none",
  buttonText: "Add to cart",
  favoriteStatus: {},
  addToCart: () => {},
  addToFavorite: () => {},
};

export default App;
