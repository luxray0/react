const updateLocalCart = function (cartState) {
    const { counterItems, buttonText } = this.state.cartState;

    localStorage.setItem(
        "cartState",
        JSON.stringify({ counterItems, buttonText })
    );
    toggleCounterItemsVisible.call(this, cartState);

};
const toggleCounterItemsVisible = function (cartState) {
    this.setState((prevState) => ({
        counterItemsVisible:
            prevState.cartState.counterItems.length > 0 ? "flex" : "none",
    }));
};

const addCart = function (productId, stateProperty) {
    this.setState(
        prevState => {
            const { counterItems, buttonText } =
                prevState.cartState;

            const updatedCounterItems =
                stateProperty === "counterItems"
                    ? [...counterItems, productId]
                    : counterItems;
            const updateButtonText = {
                ...buttonText,
                [productId]: 'Added!'
            }

            const updatedCartState = {
                ...prevState.cartState,
                counterItems: updatedCounterItems,
                buttonText: updateButtonText
            };

            return { cartState: updatedCartState };
        },
        () => {
            updateLocalCart.call(this, this.state.cartState);
        }
    );
};

const removeCart = function (productId, stateProperty) {
    this.setState(
        prevState => {
            return {
                cartState: {
                    ...prevState.cartState,
                    counterItems:
                        stateProperty === "counterItems"
                            ? prevState.cartState.counterItems.filter(id => id !== productId)
                            : prevState.cartState.counterItems,
                    buttonText: {
                        ...prevState.cartState.buttonText,
                        [productId]: "Add to cart"
                    },
                }
            };
        },
        () => {
            updateLocalCart.call(this, this.state.cartState);
        }
    );
};

export { addCart, removeCart };

