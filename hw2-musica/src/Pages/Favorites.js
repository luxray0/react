const updateLocalFavorites = function (favoriteState) {
    const { counterFav, favoriteButtonColor } = this.state.favoriteState;

    localStorage.setItem(
        "favoriteState",
        JSON.stringify({ favoriteButtonColor, counterFav })
    );

    toggleCounterFavVisible.call(this, favoriteState);
};

const toggleCounterFavVisible = function (favoriteState) {
    this.setState((prevState) => ({
        counterFavVisible:
            prevState.favoriteState.counterFav.length > 0 ? "flex" : "none",
    }));
};
const addFavorite = function (productId, stateProperty) {
    this.setState(
        (prevState) => {
            const counterFav =
                stateProperty === "counterFav"
                    ? [...prevState.favoriteState.counterFav, productId]
                    : prevState.favoriteState.counterFav;
            const favoriteButtonColor = {
                ...prevState.favoriteState.favoriteButtonColor,
                [productId]: "#6ba6ff",
            };
            return {
                favoriteState: {
                    ...prevState.favoriteState,
                    counterFav,
                    favoriteButtonColor,
                },
            };
        },
        () => {
            updateLocalFavorites.call(this, this.state.favoriteState);
        }
    );
}

const removeFavorite = function (productId, stateProperty) {
    this.setState(
        (prevState) => {
            return {
                favoriteState: {
                    ...prevState.favoriteState,
                    counterFav:
                        stateProperty === "counterFav"
                            ? prevState.favoriteState.counterFav.filter(
                                (id) => id !== productId
                            )
                            : prevState.favoriteState.counterFav,
                    favoriteButtonColor: {
                        ...prevState.favoriteState.favoriteButtonColor,
                        [productId]: "#fff",
                    },
                },
            };
        },
        () => {
            updateLocalFavorites.call(this, this.state.favoriteState);

        }
    );
}



export { addFavorite, removeFavorite }