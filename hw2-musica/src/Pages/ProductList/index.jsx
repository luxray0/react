import React, { Component } from "react";
import PropTypes from "prop-types";
import ProductCard from "../../Components/ProductCard";

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    };
  }

  componentDidMount() {
    fetch("./data/products.json")
      .then((response) => response.json())
      .then((data) => {
        this.setState({
          products: data.products,
        });
      })
      .catch((error) => {
        console.log("Error>> ", error);
      });
  }

  render() {
    const {
      addToFavorite,
      addToCart,
      favoriteStatus,
      buttonText,
    } = this.props;

    const { products } = this.state;

    return (
      <div className="product__board">
        <h3>Product list:</h3>
        <div className="product__list">
          {products.map((product) => (
            <ProductCard
              key={product.id}
              {...product}
              addToFavorite={() => addToFavorite(product.id)}
              addToCart={() => addToCart(product.id)}
              favoriteStatus={favoriteStatus[product.id] || "#fff"}
              buttonText={buttonText[product.id] || "Add to cart"}
            />
          ))}
        </div>
      </div>
    );
  }
}

ProductList.propTypes = {
  addToFavorite: PropTypes.func.isRequired,
  addToCart: PropTypes.func.isRequired,
  favoriteStatus: PropTypes.object.isRequired,
  buttonText: PropTypes.string.isRequired,
};

ProductList.defaultProps = {
  favoriteStatus: {},
  buttonVisible: {},
};

export default ProductList;
